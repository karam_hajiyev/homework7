package az.ibatech;

import java.util.Arrays;
import java.util.Objects;

public class Family {

    protected Human mother, father;
    protected Human[] children;
    protected Pet pet;


    public Family() {
    }

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.children = new Human[]{};
    }


    public int countFamily() {
        return 2 + children.length;
    }

    public void addChild(Human child) {
        Human[] temp = new Human[children.length + 1];
        for (int i = 0; i < children.length; i++) {
            temp[i] = children[i];
        }
        temp[children.length] = child;
        this.children = temp;
        System.out.println("The child has been added.");
    }

    public boolean deleteChild(int index) {
        if (index > children.length || index < 0) {
            System.out.println("You write wrong index");
            return false;
        }

        Human[] temp = new Human[this.children.length - 1];
        for (int i = 0, j = 0; j < children.length - 1; i++, j++) {
            if (j != index) {
                temp[i] = children[j];
            }
        }
        this.children = temp;
        System.out.println("The child has been deleted.");
        return true;
    }

    public boolean deleteChild(Human child) {
        Human[] temp = new Human[this.children.length - 1];
        for (int i = 0, index = 0; i < this.children.length - 1; i++, index++) {
            if (!children[i].equals(child))
                temp[index] = children[i];
        }
        this.children = temp;
        System.out.println("The child has been deleted.");
        return true;
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Family)) return false;
        Family family = (Family) o;
        return getMother().equals(family.getMother()) && getFather().equals(family.getFather()) && Arrays.equals(getChildren(), family.getChildren()) && getPet().equals(family.getPet());
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(getMother(), getFather(), getPet());
        result = 31 * result + Arrays.hashCode(getChildren());
        return result;
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + Arrays.toString(children) +
                ", pet=" + pet +
                '}';
    }
}
