package az.ibatech;


public class Main {
    public static void main(String[] args) {
        Woman mother = new Woman("Nigar", "Qasimova", 1999);
        Man father = new Man("Isa", "Qasimov", 1994);
        father.repairCar();
        mother.makeUp();

        Family familyQasimov = new Family(mother, father);
        mother.setFamily(familyQasimov);
        father.setFamily(familyQasimov);

        Woman esma = new Woman("Arzu", "Qasimova", 2020, 160);

        System.out.println(esma.monday.name());     esma.setSchedule("Monday", "go to gym");
        System.out.println(esma.tuesday.name());    esma.setSchedule("Sunday", "do shopping");
        System.out.println(esma.wednesday.name());  esma.setSchedule("Wednesday", "do workout");
        System.out.println(esma.thursday.name());   esma.setSchedule("Friday", "read e-mails");
        System.out.println(esma.friday.name());     esma.setSchedule("Saturday", "do home work");
        System.out.println(esma.saturday.name());   esma.setSchedule("Thursday", "visit grandparents");
        System.out.println(esma.sunday.name());     esma.setSchedule("Tuesday", "do household");

        familyQasimov.addChild(esma);
        esma.setFamily(familyQasimov);

        Dog esmasPet = new Dog("Rex",
                5, 49, new String[]{"eat, drink, sleep"});
        familyQasimov.setPet(esmasPet);
        esma.greetPet();
        esma.describePet();
        esmasPet.respond();
        esmasPet.eat();
        esmasPet.foul();

        System.out.println(esmasPet.toString());

        System.out.println(esma.toString());

        Man child1 = new Man();
        Woman child2 = new Woman();
        Man child3 = new Man();
        familyQasimov.addChild(child1);
        familyQasimov.addChild(child2);
        familyQasimov.addChild(child3);
        familyQasimov.countFamily();

        System.out.println();

        familyQasimov.deleteChild(2);
        familyQasimov.countFamily();

        System.out.println();

        familyQasimov.deleteChild(child3);
        familyQasimov.countFamily();
    }
}

