package az.ibatech;


public abstract class Human {

    protected String name, surname;
    protected int year, iq;
    protected Family family;

    protected DayOfWeek monday = DayOfWeek.MONDAY;

    protected DayOfWeek tuesday = DayOfWeek.TUESDAY;

    protected DayOfWeek wednesday = DayOfWeek.WEDNESDAY;

    protected DayOfWeek thursday = DayOfWeek.THURSDAY;

    protected DayOfWeek friday = DayOfWeek.FRIDAY;

    protected DayOfWeek saturday = DayOfWeek.SATURDAY;

    protected DayOfWeek sunday = DayOfWeek.SUNDAY;


    // constructors
    public Human() {
    }

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, int iq) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
    }

    // methods
    public void describePet() {
        System.out.print("I have a " + family.getPet().getSpecies() +
                ", he is " + family.getPet().getAge() + " years old, he is ");
        System.out.println((family.getPet().getTrickLevel() > 50) ? "very sly." : "almost not sly.");
    }

    // setters
    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public void setSchedule(String schedule, String doing) {
        switch (schedule.toUpperCase()) {
            case "MONDAY":
                this.monday.setLabel(doing);
                break;
            case "TUESDAY":
                this.tuesday.setLabel(doing);
                break;
            case "WEDNESDAY":
                this.wednesday.setLabel(doing);
                break;
            case "THURSDAY":
                this.thursday.setLabel(doing);
                break;
            case "FRIDAY":
                this.friday.setLabel(doing);
                break;
            case "SATURDAY":
                this.saturday.setLabel(doing);
                break;
            case "SUNDAY":
                this.sunday.setLabel(doing);
                break;
            default:
                System.out.println("Please write correct days of the week");
                break;
        }
    }

    // getters
    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getYear() {
        return year;
    }

    public int getIq() {
        return iq;
    }

    public Family getFamily() {
        return family;
    }

    public String getSchedule() {
        return monday + " " + monday.getLabel() + " "
                + tuesday + " " + tuesday.getLabel() + " "
                + wednesday + " " + wednesday.getLabel() + " "
                + thursday + " " + thursday.getLabel() + " "
                + friday + " " + friday.getLabel() + " "
                + saturday + " " + saturday.getLabel() + " "
                + sunday + " " + sunday.getLabel();
    }
}





