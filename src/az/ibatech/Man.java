package az.ibatech;

public final class Man extends Human {
    public Man() {
    }

    public Man(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Man(String name, String surname, int year, int iq) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
    }

    public void repairCar() {
        System.out.println("It's time to repair car.");
    }

    public void greetPet() {
        System.out.println("Hello, " + family.getPet().getNickname() + '.');
    }
}
