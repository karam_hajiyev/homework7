package az.ibatech;

import java.util.Arrays;

public abstract class Pet {
    protected String nickname;
    protected String[] habits;
    protected int age, trickLevel;
    protected Species species;

    public Pet() {
    }

    public Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
        setSpecies(species);
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public Species getSpecies() {
        return species;
    }

    public void eat() {
        System.out.println("I am eating.");
    }

    public void respond() {
        System.out.println("Hello, owner. I am - " + nickname + ". I miss you!");
    }

    public void setSpecies(String species) {
        String modSpecies = species.trim().toUpperCase();
        Species tmpSpecies = Species.valueOf(modSpecies);
        if (tmpSpecies == Species.DOG) {
            this.species = Species.DOG;
        } else if (tmpSpecies == Species.DOMESTICCAT) {
            this.species = Species.DOMESTICCAT;
        } else if (tmpSpecies == Species.ROBOCAT) {
            this.species = Species.ROBOCAT;
        } else if (tmpSpecies == Species.FISH) {
            this.species = Species.FISH;
        } else if (tmpSpecies == Species.UNKNOWN) {
            this.species = Species.UNKNOWN;
        }
    }


    @Override
    public String toString() {
        return species + "{" +
                "nickname = '" + nickname + '\'' +
                ", age = " + age +
                ", trickLevel = " + trickLevel +
                ", habits = " + Arrays.toString(habits) + "}";
    }
}


